let email = $("#email");
let username = $("#username");
let password = $("#password");
let dob = $("#dob");
let address = $("#address");
let role = $("#em_role");
const id = localStorage.getItem("id");
const token = localStorage.getItem("token");

addEmployee = () => {
    if (email.val().trim() != "" && username.val().trim() != "" && password.val().trim() != "" && dob.val().trim() != "" && address.val().trim() != "" && role.val().trim() != "" && role.val().trim() != "none") {
        let data = {
            id: id,
            token: token,
            email: email.val().trim(),
            username: username.val().trim(),
            password: password.val().trim(),
            dob: dob.val().trim(),
            address: address.val().trim(),
            role: role.val().trim()
        };

        let createUser = $.post({
            url: "http://localhost/webbe/api/createuser",
            data: JSON.stringify(data)
        });

        createUser.done(data => {
            console.log(data.msg);
            (data.msg == 'ok') ? window.location.href = "/Page/employee/em_mana/index.html": alert("Thêm nhân viên lỗi!");
        });

        createUser.fail(() => {
            console.log("Fail");
        });
        // console.log(data);
    } else {
        alert("Nhập đầy đủ các trường");
    }
};