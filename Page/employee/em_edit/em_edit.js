const id = localStorage.getItem("id");
const token = localStorage.getItem("token");
let username = $("#username");
let user_dob = $("#user_dob");
let address = $("#address");
let user_role = $("#user_role");
let href = window.location.href;
const idReg = /usr_id/;
let em_id = href.slice(idReg.exec(href).index + 7);
let employee_id = {
    "em_id": em_id.toString()
}
let admin_info = (em_id) => {
    let info;
    if (em_id) {
        info = {
            id: id,
            token: token,
            em_id: em_id
        }
    } else {
        info = {
            id: id,
            token: token
        }
    }
    return info;
};
//===========================================
let getEmInfo = $.post({
    url: "http://localhost/webbe/api/alluser",
    data: JSON.stringify(admin_info(em_id))
});
getEmInfo.done(data => {
    // console.log(data.body[0]);
    username.val(data.body[0].username);
    user_dob.val(data.body[0].dob);
    address.val(data.body[0].address);
    user_role.val(data.body[0].role);
});
getEmInfo.fail(() => {
    alert("Đã xảy ra lỗi!");
});
//===========================================
editEm = () => {
    if (username.val().trim() != "" && user_dob.val().trim() != "" && address.val().trim() != "" && user_role.val().trim() != "none" && user_role.val().trim() != "") {
        let newData = {
            user_id: id,
            em_id: em_id,
            token: token,
            username: username.val().trim(),
            dob: user_dob.val().trim(),
            role: user_role.val().trim(),
            address: address.val().trim()
        };

        let updateData = $.post({
            url: "http://localhost/webbe/api/updateuser",
            data: JSON.stringify(newData)
        });

        updateData.done(data => {
            (data.msg == 'ok') ? window.location.href = "/Page/employee/em_mana/index.html": alert("Cập nhật lỗi!");
        });

        updateData.fail(() => {
            console.log("Fail");
        });
    } else
        alert("Nhập đầy đủ thông tin!");
    return false;
};