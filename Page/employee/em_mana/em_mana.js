const token = localStorage.getItem("token").trim();
const id = localStorage.getItem("id").trim();
const role = ["Admin", "Nhân viên bán hàng", "Nhân viên kho"];
let admin_info = (em_id) => {
    let info;
    if (em_id) {
        info = {
            id: id,
            token: token,
            em_id: em_id
        }
    } else {
        info = {
            id: id,
            token: token
        }
    }
    return info;
};
let employeeList = $("#employeeList");
let createEmTag = (obj) => {
    return (`
    <tr id="${obj.user_id}">
        <th scope="row">${obj.email}</th>
        <td><a href="/Page/employee/em_edit/index.html?usr_id=${obj.user_id}">${obj.username}</a></td>
        <td>${obj.dob}</td>
        <td>${obj.address}</td>
        <td>${role[obj.role]}</td>
    </tr>
    `);
};

//==============================================
let getEm = $.post({
    url: "http://localhost/webbe/api/alluser",
    data: JSON.stringify(admin_info())
});
getEm.done(data => {
    data.body.map(item => {
        employeeList.append(createEmTag(item));
    });
    // console.log(data.body)
});
getEm.fail(() => {
    alert("Đã xảy ra lỗi!");
});
//==============================================
$("#searchEmployee").on("keyup", function() {
    let value = $(this).val().toLowerCase();
    $("#employeeList tr").filter(function() {
        $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
});