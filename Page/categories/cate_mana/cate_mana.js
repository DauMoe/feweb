let cateList = $("#cateList");
let createCatesTagTable = (obj) => {
    return (
        `<tr id="${obj.cate_ID}">
            <th scope="row">${obj.cate_name}</th>
            <td>${obj.cate_code}</td>
            <td><button class="btn btn-warning" onclick=delPro("${obj.cate_ID}")>Xóa</button></td>
        </tr>`
    );
};

//======================================================
let getAllCates = $.get({
    url: "http://localhost/webbe/api/allcates"
});

getAllCates.done(data => {
    data.body.map(item => {
        cateList.append(createCatesTagTable(item));
    });
});

getAllCates.fail(() => {
    alert("Lấy dữ liệu danh mục thất bại!");
});
//======================================================
$("#searchCates").on("keyup", function() {
    let value = $(this).val().toLowerCase();
    $("#cateList tr").filter(function() {
        $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
});