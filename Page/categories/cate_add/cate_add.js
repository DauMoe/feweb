let cate_name = $("#cate_name");
let cate_code = $("#cate_code");

addCates = () => {
    if (cate_name.val().trim() != "" && cate_code.val().trim() != "") {
        let cate = {
            cate_name: cate_name.val().trim(),
            cate_code: cate_code.val().trim()
        };
        let createCate = $.post({
            url: "http://localhost/webbe/api/createcate",
            data: JSON.stringify(cate)
        });
        createCate.done(data => {
            (data.msg == 'ok') ? window.location.href = "/Page/categories/cate_mana/index.html": alert("Thêm danh mục thất bại!");
        });
        createCate.fail(() => {
            alert("Tạo danh mục thất bại!");
        });
    } else
        alert("Điền đầy đủ các trường");
    return false;
};