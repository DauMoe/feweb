//Doc: https://www.chartjs.org/docs/latest/charts/doughnut.html
const token = localStorage.getItem("token").trim();
const id = localStorage.getItem("id").trim();
const role = ["Admin", "Nhân viên bán hàng", "Nhân viên kho"];
let admin_info = (em_id) => {
    let info;
    if (em_id) {
        info = {
            id: id,
            token: token,
            em_id: em_id
        }
    } else {
        info = {
            id: id,
            token: token
        }
    }
    return info;
};
let catetegories = [];
let cates_quantity = [];
let products_name = [];
let products_quantity = [];
let products_price = [];
let user_quantity = [0, 0, 0];

let getProList = $.ajax({
    type: "POST",
    url: "http://localhost/webbe/api/allproducts"
});

getProList.done(data => {
    let index = 0;
    // console.log(data.body);
    for (let i = 0; i < data.body.length; i++) {
        if (i == 0) {
            catetegories.push(data.body[0].pro_cate);
            cates_quantity.push(Number(data.body[0].pro_quantity));
        } else {
            if (data.body[i - 1].pro_cate != data.body[i].pro_cate) {
                catetegories.push(data.body[i].pro_cate);
                cates_quantity.push(Number(data.body[i].pro_quantity));
                index++;
            } else {
                cates_quantity[index] += Number(data.body[i].pro_quantity);
            }
        }
    }
    data.body.map(item => {
        products_name.push(item.pro_id);
        products_quantity.push(item.pro_quantity);
        products_price.push(Number(item.pro_purchaseprice) - Number(item.pro_saleprice));
    });
});

getProList.fail(() => {
    console.log('error');
});

//=====================================================
let getEm = $.post({
    url: "http://localhost/webbe/api/alluser",
    data: JSON.stringify(admin_info())
});
getEm.done(data => {
    data.body.map(user => {
        if (user.role == "0") {
            user_quantity[0] += 1;
        }
        if (user.role == "1") {
            user_quantity[1] += 1;
        }
        if (user.role == "2") {
            user_quantity[2] += 1;
        }
    });
});
getEm.fail(() => {
    alert("Đã xảy ra lỗi!");
});
//=====================================================
setTimeout(() => {
    // console.log(catetegories);
    // console.log(cates_quantity);
    //draw pie chart
    var pie = document.getElementById('pie_chart').getContext('2d');
    var myChart = new Chart(pie, {
        type: 'doughnut',
        data: {
            labels: catetegories,
            datasets: [{
                data: cates_quantity,
                backgroundColor: [
                    'rgba(255, 99, 132)',
                    'rgba(54, 162, 235)',
                    'rgba(255, 206, 86)',
                    'rgba(75, 192, 192)',
                    'rgba(153, 102, 255)',
                    'rgba(255, 159, 64)'
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }]
        }
    });

    //line
    // console.log(products_price);
    var radar = document.getElementById('radar_chart').getContext('2d');
    var myChart = new Chart(radar, {
        type: 'radar',
        data: {
            labels: products_name,
            datasets: [{
                label: "Lợi nhuận trên mỗi sản phẩm bán ra",
                data: products_price,
                backgroundColor: [
                    'rgba(255, 99, 132, 0.3)'
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }]
        }
    });

    //radar
    var bar = document.getElementById('bar_chart').getContext('2d');
    var bar_chart = new Chart(bar, {
        type: 'bar',
        data: {
            labels: products_name,
            datasets: [{
                label: "Số lượng sản phẩm",
                data: products_quantity,
                backgroundColor: [
                    'rgba(255, 99, 132)',
                    'rgba(54, 162, 235)',
                    'rgba(255, 206, 86)',
                    'rgba(75, 192, 192)',
                    'rgba(153, 102, 255)',
                    'rgba(255, 159, 64)'
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                xAxes: [{
                    gridLines: {
                        offsetGridLines: true
                    }
                }]
            }
        }
    });
    var pie = document.getElementById('pieEm_chart').getContext('2d');
    var myChart = new Chart(pie, {
        type: 'pie',
        data: {
            labels: role,
            datasets: [{
                data: user_quantity,
                backgroundColor: [
                    'rgba(255, 99, 132)',
                    'rgba(54, 162, 235)',
                    'rgba(255, 206, 86)',
                    'rgba(75, 192, 192)',
                    'rgba(153, 102, 255)',
                    'rgba(255, 159, 64)'
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }]
        }
    });
}, 1000);