let email = $("#login-email");
let password = $("#login-password");
let emailAlert = $("#login-email-alert");
let passwordAlert = $("#login-password-alert");
const roleName = ["Admin", "Nhân viên bán hàng", "Nhân viên kho"];

if (localStorage.getItem("username") && localStorage.getItem("token")) {
    window.location.href = "/Page/products/pro_mana/index.html";
} else {
    localStorage.clear();
}

login = () => {
    let sendLoginData = $.ajax({
        url: "http://localhost/webbe/api/login",
        type: "POST",
        data: JSON.stringify({
            email: email.val().trim(),
            password: password.val().trim()
        })
    });
    sendLoginData.done(data => {
        if (data.msg == "ok") {
            localStorage.setItem("username", data.username);
            localStorage.setItem("id", data.id);
            localStorage.setItem("role", roleName[Number(data.role)]);
            localStorage.setItem("token", data.token);
            (data.role == "0") ? window.location.href = "/Page/home/index.html": window.location.href = "/Page/products/pro_mana/index.html"
        }
    });

    sendLoginData.fail(() => {
        console.log("fail");
    });
    return false;
}