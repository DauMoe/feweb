let pro_name = $("#pro_name");
let pro_saleprice = $("#saleprice");
let pro_purchaseprice = $("#purchaseprice");
let pro_cates = $("#product_cates");
let pro_quantity = $("#quantity");
let pro_image = $("#pro_image");
let preview = document.querySelector("#proImg_preview");
let base64Img;

previewFile = () => {
    const file = document.querySelector('input[type=file]').files[0];
    const reader = new FileReader();

    reader.onload = function() {
        // convert image file to base64 string
        base64Img = reader.result;
        preview.src = reader.result;
    };
    reader.readAsDataURL(file);
};
//===========================================================
let createCateTag = (obj) => {
    return (
        `<option value="${obj.cate_code}">${obj.cate_name}</option>`
    );
}
let getCate = $.get({
    url: "http://localhost/webbe/api/allcates"
});
getCate.done(data => {
    data.body.map(item => {
        pro_cates.append(createCateTag(item));
    });
});

getCate.fail(() => {
    alert("Lỗi cập nhật danh mục!")
});
//===========================================================
addProduct = () => {
    if (pro_cates.val() != "none") {
        let data = {
            pro_name: pro_name.val().trim(),
            pro_quantity: pro_quantity.val().trim(),
            pro_categories: pro_cates.val().trim(),
            pro_saleprice: pro_saleprice.val().trim(),
            pro_purchaseprice: pro_purchaseprice.val(),
            pro_image: base64Img
        };
        let createPro = $.ajax({
            type: "POST",
            url: "http://localhost/webbe/api/createproduct",
            contentType: 'application/json',
            data: JSON.stringify(data)
        });

        createPro.done(data => {
            (data.msg == 'ok') ? window.location.href = "/Page/products/pro_mana/index.html": alert("Tạo sản phẩm lỗi!");
        });

        createPro.fail(() => {
            console.log("Fail");
        });
    } else {
        alert("Điền đầy đủ các trường!");
    }
    return false;
}