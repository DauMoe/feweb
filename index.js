let dropdown = document.getElementsByClassName("dropdown-btn");
let logout = $("#logout");
let homepage = $("#homepage");

//============================== Logout ===================================
logout.click(() => {
    localStorage.clear();
    window.location.href = "/Page/Login/login.html";
    console.log("logout");
});

//============================== Navigator ===================================
if (localStorage.getItem("username").trim() != null && localStorage.getItem("role").trim() != null && localStorage.getItem("token").trim() != null) {
    $("#hello").text(`Chào mừng ${localStorage.getItem("username").trim()} quay trở lại!`);
    $("#role").text(`${localStorage.getItem("role").trim()}`);
    if (localStorage.getItem("role").toLowerCase() != "admin") {
        homepage.css("display", "none");
    }
}

//============================== Dropdown ===================================
for (let i = 0; i < dropdown.length; i++) {
    dropdown[i].addEventListener("click", function() {
        let dropdownContent = this.nextElementSibling;
        if (dropdownContent.style.display === "block") {
            dropdownContent.style.display = "none";
        } else {
            dropdownContent.style.display = "block";
        }
    });
}

//============================== Export Excel ===================================
let exportTableToExcel = (tableID, filename = '') => {
    var downloadLink;
    var dataType = 'application/vnd.ms-excel';
    var tableSelect = document.getElementById(tableID);
    var tableHTML = tableSelect.outerHTML.replace(/ /g, '%20');

    // Specify file name
    filename = filename ? filename + '.xls' : 'excel_data.xlsx';

    // Create download link element
    downloadLink = document.createElement("a");
    document.body.appendChild(downloadLink);

    if (navigator.msSaveOrOpenBlob) {
        var blob = new Blob(['\ufeff', tableHTML], {
            type: dataType
        });
        navigator.msSaveOrOpenBlob(blob, filename);
    } else {
        // Create a link to the file
        downloadLink.href = 'data:' + dataType + ', ' + tableHTML;

        // Setting the file name
        downloadLink.download = filename;

        //triggering the function
        downloadLink.click();
    }
}